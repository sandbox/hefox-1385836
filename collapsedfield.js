(function ($) {
Drupal.behaviors.collapsedfield_text ={ 
    attach:
      function (context, settings) {
        // Get all the settings, and save the limits in the fields.
        var element = {};
        for (var id in Drupal.settings.collapsedfield_text) {
          var $element = $('#' + id);
          if ($element.length) {
            $element.hide();
            var link_id = id + '-collapsedfield-text';
            $element.before('<div class="collapsedfield-text-wrapper"><a href="#" id="' + link_id + '">' + Drupal.settings.collapsedfield_text[id] + '</a><br/></div>');
            $('#' + link_id, context).bind('click', {field_id: id}, function(event) {
              $(this).parent('.collapsedfield-text-wrapper').remove();
              $('#' + event.data.field_id).show();
              return false;
            });
          }
        }
      }
  };
}(jQuery));

